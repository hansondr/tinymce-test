Rails.application.routes.draw do
  root to: 'pages#index'
  resources :pages

  get '*path', to: 'user_defined_page#index'
end
