class UserDefinedPageController < ApplicationController
  def index
    redirect_to_user_defined_page || raise_error
  end

  private

  def redirect_to_user_defined_page
    @page = Page.where(uri: params[:path]).first

    if @page.present?
      puts "PAGE: #{@page.inspect}"
      render "pages/show"
    else
      nil
    end
  end

  def raise_error
    raise ActionController::RoutingError, "No route matches [GET] \"/#{params[:path]}\""
  end
end
