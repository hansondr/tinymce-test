class Page < ApplicationRecord
  validates :uri, uniqueness: { case_sensitive: false }

  scope :all_in_menu, -> { where(in_menu: true) }
  scope :ordered_by_title, -> { order(title: :asc) }
end
