class CreatePages < ActiveRecord::Migration[5.1]
  def change
    create_table :pages do |t|
      t.string :title
      t.string :body
      t.string :uri, :index
      t.boolean :in_menu, default: false

      t.timestamps
    end
  end
end
